package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os/exec"
	"syscall"

	"github.com/kballard/go-shellquote"
	"github.com/logrusorgru/aurora"
	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/kubesec/v2/convert"
	"gitlab.com/gitlab-org/security-products/analyzers/kubesec/v2/plugin"
)

const (
	pathOutput = "kubesec.json"
)

var vulnerabilities = make([]convert.KubesecOutput, 0)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		cli.StringFlag{
			Name:   "helm-charts-path",
			Usage:  "Optional helm charts path where to run helm on before runing kubesec",
			EnvVar: "KUBESEC_HELM_CHARTS_PATH",
		},
		cli.StringFlag{
			Name:   "helm-options",
			Usage:  "Specify helm options",
			EnvVar: "KUBESEC_HELM_OPTIONS",
		},
	}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	var helmChartsPath string = c.String("helm-charts-path")
	if helmChartsPath != "" {
		var cmdOut []byte
		var err error
		var args []string

		args = append(args, "template", helmChartsPath)
		if c.String("helm-options") != "" {
			helmOptions, err := shellquote.Split(c.String("helm-options"))
			if err != nil {
				fmt.Println(aurora.Red("Invalid helm options"))
				return nil, err
			}
			args = append(args, helmOptions...)
		}
		helmCmd := exec.Command("helm", args...)

		helmCmd.Dir = path
		cmdOut, err = helmCmd.CombinedOutput()
		if err != nil {
			fmt.Println(aurora.Red("An error occurred while running helm template."))
			fmt.Println(aurora.Red(string(cmdOut)))
			return nil, err
		}
		ioutil.WriteFile(path+"/sast_helm_generated_manifest.yaml", cmdOut, 0644)
	}
	fmt.Println("Searching", path, "for Kubernetes manifests.")
	manifests := plugin.WalkForYaml(path)
	for _, manifestPath := range manifests {
		var cmdOut []byte
		var err error

		cmdName := "kubesec"
		args := []string{
			"scan",
			path + manifestPath,
		}

		cmd := exec.Command(cmdName, args...)
		cmdOut, err = cmd.CombinedOutput()

		// This command results in an an exit status 2,
		// but it does return the json as expected.
		if err != nil {
			if exitError, ok := err.(*exec.ExitError); ok {
				waitStatus := exitError.Sys().(syscall.WaitStatus)
				if waitStatus.ExitStatus() != 2 {
					// An error occurred while running kubesec
					// (see https://github.com/controlplaneio/kubesec/blob/74a92d6ab378f8334ef4140c7bd5caeb4f22a265/cmd/kubesec/main.go#L30 )
					fmt.Println(aurora.Red("An error occurred while running kubesec."))
					return nil, err
				}
			}
		}

		var vulnerability = convert.KubesecOutput{
			Filepath: manifestPath,
			Findings: cmdOut,
		}

		vulnerabilities = append(vulnerabilities, vulnerability)
	}

	file, err := json.MarshalIndent(vulnerabilities, "", " ")

	return ioutil.NopCloser(bytes.NewReader(file)), err
}
