FROM golang:1.13-alpine AS build
ENV CGO_ENABLED=0
WORKDIR /go/src/app
COPY . .
RUN go build -o analyzer

# The "app" user (from the kubesec/kubesec image) doesn't have permission to create a
# file in /etc/ssl/certs, this RUN command creates a file that the analyzer can add
# additional ca certs to trust.
RUN touch /ca-cert-additional-gitlab-bundle.pem

FROM alpine/helm:2.16.1 as helm
FROM kubesec/kubesec:2504119

COPY --from=helm /usr/bin/helm /usr/bin/helm
ENV PATH="/home/app:${PATH}"
COPY --from=build /go/src/app/analyzer /
COPY --from=build --chown=app:app /ca-cert-additional-gitlab-bundle.pem /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem

ENTRYPOINT []
CMD ["/analyzer", "run"]
