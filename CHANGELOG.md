# Kubesec analyzer changelog

## v2.2.4
- Update kubesec to [v2.4.0](https://github.com/controlplaneio/kubesec/releases/tag/v2.4.0) (!17)

## v2.2.3
- Add support for helm charts (!8 @agixid)

## v2.2.2
- Remove `location.dependency` from the generated SAST report (!16)

## v2.2.1
- Use Alpine as builder image (!13)

## v2.2.0
- Add `id` field to vulnerabilities in JSON report (!10)

## v2.1.0
- Add support for custom CA certs (!7)

## v2.0.1
- Move away from use of CI_PROJECT_DIR variable

## v2.0.0
- Initial release
