module gitlab.com/gitlab-org/security-products/analyzers/kubesec/v2

go 1.12

require (
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51
	github.com/logrusorgru/aurora v0.0.0-20191017060258-dc85c304c434
	github.com/urfave/cli v1.22.1
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.10.0
)
